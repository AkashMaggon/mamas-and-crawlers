var express = require("express");
const router = express.Router();
var contactCtrl = require("../controllers/contact");

router.route("/").get(contactCtrl.contactUs);

module.exports = router;
