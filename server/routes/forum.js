var express = require("express");
const router = express.Router();
var forumCtrl = require("../controllers/forums");

router.route("/getBookingForm").get(forumCtrl.getBookingForm);

router.route("/createForum").post(forumCtrl.createForum);

module.exports = router;
