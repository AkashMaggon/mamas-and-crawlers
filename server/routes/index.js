var forum = require("./forum");
var contact = require("./contact");
var express = require("express");
const router = express.Router();
const hbs = require("hbs");

router.get("/health-check", (req, res) => {
  res.send("OK");
});

router.get("/", (req, res) => {
  res.render("index.hbs");
});

router.get("/index.html", (req, res) => {
  res.render("index.hbs");
});

router.get("/photo-1.html", (req, res) => {
  res.render("photos.hbs");
});

router.get("/pricing1.html", (req, res) => {
  res.render("pricing.hbs");
});

router.get("/testimonial-1.html", (req, res) => {
  res.render("testimonial.hbs");
});

router.get("/bookingform-1.html", (req, res) => {
  res.render("bookingform.hbs");
});

router.get("/form-1.html", (req, res) => {
  res.render("contactus.hbs");
});

router.use("/forum", forum);

router.use("/contact", contact);

module.exports = router;
