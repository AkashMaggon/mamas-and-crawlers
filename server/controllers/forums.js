const mongoose = require("mongoose");
const Forum = require("../models/forums");

function createForum(req, res, next) {
  var forumAuthor = req.body.forumAuthor;
  var checkoutdate = req.body.checkoutdate;
  var checkindate = req.body.checkindate;
  var reason = req.body.reason;
  var email = req.body.email;
  var message = req.body.message;
  var forum = new Forum({
    forumAuthor: forumAuthor,
    checkindate: checkindate,
    checkoutdate: checkoutdate,
    email: email,
    reason: reason,
    message: message
  });

  forum.save().then(doc => {
    console.log(doc);
    doc.save();
    res.send("Forum Successfully Saved!");
  });
}

function getBookingForm(req, res, next) {
  res.send("Booking form");
}

module.exports = { createForum, getBookingForm };
