const mongoose = require("mongoose");

var Schema = mongoose.Schema;
const forumSchema = new Schema({
  forumAuthor: { type: String, default: null },
  checkoutdate: { type: String, default: null },
  checkindate: { type: String, default: null },
  reason: { type: String, default: null },
  email: { type: String, default: null },
  message: { type: String, default: null }
});

module.exports = mongoose.model("Forum", forumSchema);
